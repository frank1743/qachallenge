const login01 = require("../locators/correo");
const { I } = inject();

module.exports = {
 
    poner_correo_pass(email, password){
     // I.switchToNextTab();
     // I.seeInTitle("Login");
      //I.seeElement(login01.EMAIL);
      I.amOnPage('/login');
      I.wait(2);
      I.fillField(login01.EMAIL, email);
      I.fillField(login01.CONTRASENA, password);
      
  },

    dar_clic(){
    
    I.click(login01.BOTON_LOGIN);
    

 },

    mensaje(){
    
     I.seeElement(login01.MENSAJE_OK)
     I.see("LOGIN VALID")

}
  
}