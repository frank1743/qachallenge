const { setHeadlessWhen,
        setWindowSize } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run



//const URL = process.env.CODECEPTJS;

exports.config = {
  tests: './*_test.js',
  output: './e2e/src/reports/output',
  helpers: {
    WebDriver: {
      url: "http://localhost:4000/login",
      browser: "chrome", //process.env.CODECEPTJS_BROWSER,
      windowSize: "maximize" 
      },
      REST: {
        endpoint: '',
        defaultHeaders: {
          'Content-Type': 'application/json'
        }
      },
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'WebdriverIO',
  gherkin: {
    /*features: ['./e2e/src/web/features/*.feature',
              './e2e/src/apis/features/*.feature'],
    steps: ['./e2e/src/web/steps/*.js',
            './e2e/src/apis/steps/*.js'] */
            features: './e2e/src/web/features/*.feature',
            steps: './e2e/src/web/steps/*.js'
  },
  plugins: {
    autoDelay: {
      enabled: true,
      delayBefore: 2000,
      delayAfter: 2000,
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true,
      uniqueScreenshotNames: true,
      fullPageScreenshots: true
    },
    wdio: {
      enabled:true,
      services:['selenium-standalone']
    },
    allure: {
      enabled: true,
      outputDir: "./e2e/src/reports/allure"
    } 

  }
}