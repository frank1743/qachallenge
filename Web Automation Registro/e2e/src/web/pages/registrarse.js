const registro = require("../locators/correo");
const { I } = inject();

module.exports = {
 
    poner_correo_pass(email, password){
     
      I.amOnPage('/register');
      I.wait(2);
      I.fillField(registro.EMAIL, email);
      I.fillField(registro.CONTRASENA, password);
      

  },

    dar_clic(){
    
    I.click(registro.BOTON_REGISTER);
    

 },

    mensaje(){
    
     I.seeElement(registro.MENSAJE_OK)
     I.see("SAVED")

}
  
}